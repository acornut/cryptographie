/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package substitution;

import java.io.File;

/**
 *
 * @author Antonin
 */
public interface ICipher {
    String encode(String message, String key);
    String decode(String crypted, String key);
}
