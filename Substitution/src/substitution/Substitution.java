/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package substitution;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Antonin
 */
public class Substitution implements ICipher{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(freqValues.length);
        System.out.println(freqKeys.length);
        
        
        
        Substitution m = new Substitution();
        m.getFrequencyLetters("HELLO");
        //String s = m.encodeCesar('W', "BONJOUR, COMMENT VAS-TU?");
        //String s2 = m.decodeCesar('W', s);
        /*
        //double tab[] = new double []{17.0, 8.0, 6.0};
        //System.out.println(m.generateKeyFromText(tab));
        String key = m.generateKey();
        String s = m.encode("ONNEPEUTPARDEFINITIONPASREDIGERDEPANGRAMMEDEMOINSDEVINGTSIXLETTRESMAISILSEMBLERAITQUONNEPUISSECREERUNPANGRAMMEDEMOINSDELETTRESQUENRENDANTLAPHRASEPEUINTELLIGIBLELESPANGRAMMESDEMOINSDELETTRESENVAHISDABREVIATIONSDENOMSPROPRESOUDESIGLESENTRENTDANSCETTECATEGORIEOUENCORECELUICI", key);
        System.out.println("KEY 1: " +key);
        System.out.println("Text 1: " +s);
        m.decodeFromtext(s);
                */
    }
    
    private static final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + " -.,;:\"'";
    
    private static final double freqValues[] = new double[]{17.76, 8.23, 7.68, 7.61, 7.30, 7.23, 6.81, 6.05, 5.89, 5.34, 3.60, 3.32, 3.24, 3.24, 2.72, 1.34, 1.27, 1.10, 1.06, 0.80, 0.64, 0.54, 0.21, 0.19, 0.07, 0.00};
    private static final char freqKeys[] = new char[]{'E', 'S', 'A', 'N', 'T', 'I', 'R', 'U', 'L', 'O', 'D', 'C', 'P', 'M', 'Q', 'V', 'G', 'F', 'B', 'H', 'X', 'Y', 'J', 'Z', 'K', 'W'};
   
    private char[] list = new char[]{
			'A','B','C','D','E','F','G',
			'H','I','J','K','L','M','N',
			'O','P','Q','R','S','T','U',
			'V','W','X','Y','Z'
    };

    private int[] cryp;

    public void initCesarCipher(String file){

        try {
            FileReader x = new FileReader(file);

            String holder = "";

            int ch;
            while((ch = x.read()) != -1)
                holder += Character.toString((char) ch) ;

            cryp = new int[holder.length() - 1];

            for(int p = 0; p < holder.length() - 1; ++p)
                cryp[p] = (int) holder.charAt(p);

            x.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void crack(){
        try{
            for(int x = 0; x < list.length; ++x){
                String temp = "";
                for(int y = 0; y < cryp.length; ++y){
                    int n = (cryp[y] + x);
                    if(n > 65 && n < 90) {
                            n = ((n + 1) - 65) % 25;
                    } else {
                            n = (n + 65) % 25;
                    }
                    temp += Character.toString(list[n]);
                }
                System.out.println(temp);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void crack(int key){
        try{
            String temp = "";
            for(int y = 0; y < cryp.length; ++y){
                int n = (cryp[y] + (-key));
                if(n > 65 && n < 90) {
                        n = (n - 65);
                } else {
                        n = (n + 26);
                }
                temp += Character.toString(list[n]);
            }
            System.out.println(temp);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public String encodeVigener(char key, String message){
        int asci = 0;
        int step = (int)key - 64;
        String newMessage = "";
        
        for (int i = 0; i<message.length(); i++){
            if((int)message.charAt(i) >= 65 && (int)message.charAt(i) <= 90){
                asci = (int)message.charAt(i) + step;
                if(asci > 90){
                    asci =  asci - 25;
                }
                newMessage = newMessage + (char)asci;
            }
            else{
                newMessage = newMessage + message.charAt(i);
            }
        }
        
        System.out.println("TEXT : "+newMessage);
        
        return newMessage;
    }
    
    
    
    
    
    public void getFrequencyLetters(String message){
        HashMap<Character, ArrayList> map = new HashMap<Character, ArrayList>();
        ArrayList<Integer> table = new ArrayList<Integer>();
        
        for (int i = 0; i<message.length(); i++){
            if(!map.containsKey(message.charAt(i))){
                map.put(message.charAt(i), new ArrayList());
                map.get(message.charAt(i)).add(i);
                //table.add(i);
                for (int j = i+1; j<message.length(); j++){
                    if(message.charAt(i) == message.charAt(j)){
                        map.get(message.charAt(i)).add(j);
                    }
                }
                System.out.println(map);
           }
        }
    }
    
    public void generateKeyH(){
        
    }
    
    public void saveIntoFile(String file, byte []b){
        BufferedOutputStream bs = null;

        try {

            FileOutputStream fs = new FileOutputStream(new File(file));
            bs = new BufferedOutputStream(fs);
            bs.write(b);
            bs.close();
            bs = null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bs != null) try { bs.close(); } catch (Exception e) {}
    }
    
    
    public String encodeCesar(char key, String message){
        int asci = 0;
        int step = (int)key - 64;
        String newMessage = "";
        
        for (int i = 0; i<message.length(); i++){
            if((int)message.charAt(i) >= 65 && (int)message.charAt(i) <= 90){
                asci = (int)message.charAt(i) + step;
                if(asci > 90){
                    asci =  asci - 25;
                }
                newMessage = newMessage + (char)asci;
            }
            else{
                newMessage = newMessage + message.charAt(i);
            }
        }
        
        System.out.println("TEXT : "+newMessage);
        
        return newMessage;
    }
    
    public String decodeCesar(char key, String message){
        
        int step = (int)key - 64;
        String newMessage = "";
        
        for (int i = 0; i<message.length(); i++){
            if((int)message.charAt(i) >= 65 && (int)message.charAt(i) <= 90){
                int asci = (int)message.charAt(i) - step;
                if(asci < 65){
                    asci =  asci + 25;
                }
                newMessage = newMessage + (char)asci;
            }
            else{
                newMessage = newMessage + message.charAt(i);
            }
        }
        
        System.out.println("TEXT : " + newMessage);
        
        return newMessage;
    }
    
    public HashMap<Character, Character> generateNewAlphabet(char key){
        HashMap<Character, Character> table = new HashMap<Character, Character>();
        for(int i=0 ; i < alphabet.length() ; i++){
            System.out.println(i);  
            table.put('e', alphabet.charAt(i));
        }
        return table;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public String generateKeyFromText(double [] tabPourcentage){
        HashMap<Character,Double> table = new HashMap<Character, Double>();
        for(int i=0 ; i < alphabet.length() ; i++){
            System.out.println(i);  
            table.put(alphabet.charAt(i), tabPourcentage[i]);
        }
        
        //List<Double> coll = table.values();
        
        Collections.sort(null);
        
        String newKey = "";
        
        System.out.println("KEY 2: " +newKey);
        
        return newKey;
    }
    
    public String getKeyForValue(double val, HashMap<Character,Double> table){
        
        
        return "";
    }
    
    public String generateKeyFromText2(double [] tabPourcentage){
        int indexClosestValue = 0;
        String newKey = "";
        
        for (int i = 0; i < tabPourcentage.length; i++){
            double refValue = tabPourcentage[i];
            double closestValue = freqValues[0];
            double distance = Math.abs(closestValue - refValue) ;

            for (int j = 1; j < freqValues.length; j++)
            {
                if (Math.abs(freqValues[j] - refValue) < distance)
                {
                    distance = Math.abs(freqValues[j] - refValue);
                    indexClosestValue = j;
                }
            }
            newKey = newKey + freqKeys[indexClosestValue];
        }
        
        System.out.println("KEY 2: " +newKey);
        
        return newKey;
    }
    
    
    public double [] getPurcentOfIteration(String str){
        double [] tabPourcentage = new double[alphabet.length()];
        
        for(int i=0; i < (alphabet.length()); i++){
            int occurance = count(str, alphabet.charAt(i));
            tabPourcentage[i] = (double)(occurance * 100) / str.length();
            System.out.print(tabPourcentage[i] + " ");
        }
        
        return tabPourcentage;
    }
    
    
    public static int count( final String s, final char c ) {
        final char[] chars = s.toCharArray();
        int count = 0;
        for(int i=0; i<chars.length; i++) {
            if (chars[i] == c) {
                 count++;
            }
        }
        return count;
    }

    public HashMap<Character, Character> buildConversionTableFromText(String encoded, String keyToWrite){
        String newKey = generateKeyFromText(getPurcentOfIteration(encoded));
        
        HashMap<Character,Character> table = new HashMap<Character, Character>();
        for(int i=0 ; i < alphabet.length() ; i++){
            System.out.println(i);  
            table.put(newKey.charAt(i), alphabet.charAt(i));
        }
        
        System.out.println(table);
        
        return table;
    }
     
    public String generateKey(){

        char[] charTable = alphabet.toCharArray();
        int currentIndex = charTable.length, randomIndex;
        char temporaryValue;

        // While there remain elements to shuffle...
        while (0 != currentIndex) {

            // Pick a remaining element...
            randomIndex = (int) Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = charTable[currentIndex];
            charTable[currentIndex] = charTable[randomIndex];
            charTable[randomIndex] = temporaryValue;
        }

        return new String(charTable);
    }

    private HashMap<Character, Character> buildConversionTable(String key, boolean reverse){
        HashMap<Character,Character> table = new HashMap<Character, Character>();
        for(int i=0 ; i < alphabet.length() ; i++){
            if(reverse){
                table.put(key.charAt(i), alphabet.charAt(i));
            }else{
                table.put(alphabet.charAt(i), key.charAt(i));
            }
        }
        return table;
    }

    @Override
    public String encode(String message, String key) {
        HashMap<Character,Character> table = buildConversionTable(key, false);
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0 ; i < message.length(); i++){
            stringBuilder.append(table.get(message.charAt(i)));
        }

        return stringBuilder.toString();
    }

    @Override
    public String decode(String crypted, String key) {
        HashMap<Character,Character> table = buildConversionTable(key, true);
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0 ; i < crypted.length(); i++){
            stringBuilder.append(table.get(crypted.charAt(i)));
        }

        return stringBuilder.toString();
    }
    
    public String decodeFromtext(String crypted) {
        HashMap<Character,Character> table = buildConversionTableFromText(crypted, "");
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0 ; i < crypted.length(); i++){
            stringBuilder.append(table.get(crypted.charAt(i)));
        }
        
        System.out.println("Decoded string : " + stringBuilder.toString());

        return stringBuilder.toString();
    }
}